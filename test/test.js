/* eslint-disable no-unused-vars, no-unused-expressions, prefer-arrow-callback, func-names */

import { describe } from 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import database from '../src/module/databaseModule';
import oAuthModel from '../src/module/model/oAuthModel';
import server from '../bin/www';

chai.use(chaiHttp);

const { expect } = chai;
const { request } = chai;

describe('Database module', () => {
  describe('registration', () => {
    afterEach(async () => {
      await database.removeUser(testUsername);
    });

    const testUsername = 'testUsername';
    const testPassword = 'Salasana123@';

    it('should check if user exists', async () => {
      const response = await database.registerUser(testUsername, testPassword);
      const exists = await database.userExists(testUsername);
      expect(exists).to.be.true;
    });

    it('should insert an user to database when succesful', async () => {
      const response = await database.registerUser(testUsername, testPassword);
      expect(response.command).to.equal('INSERT');
      expect(response.rowCount).to.equal(1);
    });

    it('should enforce password strength rules', async () => {
      const insecurePassword = 'Salasana';

      await database.registerUser(testUsername, insecurePassword).catch((error) => {
        expect(error).to.exist.and.to.be.instanceOf(Error).and.have.property('message', 'Password does not fulfill strength requirements.');
      });
    });
  });

  describe('login', () => {
    const testUsername = 'testUsername';
    const testPassword = 'Salasana123@';

    before(async () => {
      await database.registerUser(testUsername, testPassword);
    });

    after(async () => {
      await database.removeUser(testUsername);
    });

    it('should return true when password is correct', async () => {
      const accepted = await database.authenticateUser(testUsername, testPassword);
      expect(accepted).to.be.true;
    });

    it('should return false when password is incorrect', async () => {
      const wrongPassword = 'WrongPassword123@';
      const accepted = await database.authenticateUser(testUsername, wrongPassword);
      expect(accepted).to.be.false;
    });

    it('should return user object when user exists and password is correct', async () => {
      const userObject = await database.getUser(testUsername, testPassword);

      expect(userObject.rows[0].username).to.equal(testUsername);
    });

    it('should return reject promise with error when info is incorrect', async () => {
      await database.getUser(testUsername, 'WrongPassword123@').catch((error) => {
        expect(error).to.exist.and.to.be.instanceOf(Error).and.have.property('message', 'User authentication failed.');
      });
    });
  });

  describe('user data modification', () => {
    const testUsername = 'testUsername';
    const testPassword = 'Salasana123@';

    beforeEach(async () => {
      await database.registerUser(testUsername, testPassword);
    });

    afterEach(async () => {
      await database.removeUser(testUsername);
    });

    it('should save category to database when succesful', async () => {
      const testCategoryName = 'unitTestCategory123456TestTest5555';

      const userResponse = await database.getUser(testUsername, testPassword);

      const response = await database.addCategory(userResponse.rows[0].id, testCategoryName);

      expect(response.command).to.equal('INSERT');
      expect(response.rowCount).to.equal(1);
    });

    it('should save sub_category to database when succesful', async () => {
      const testName = 'test sub category';
      const userResponse = await database.getUser(testUsername, testPassword);
      const userId = userResponse.rows[0].id;
      const testCategoryName = 'unitTestCategory123456TestTest5555';
      await database.addCategory(userResponse.rows[0].id, testCategoryName);
      const categoryResponse = await database.getCategory(userId, testCategoryName);
      const categoryId = categoryResponse.rows[0].id;

      const response = await database.addSubCategory(testName, userId, categoryId);

      expect(response.command).to.equal('INSERT');
      expect(response.rowCount).to.equal(1);
    });

    it('should update sub_category in database when succesful', async () => {
      const testName = 'test sub category';
      const userResponse = await database.getUser(testUsername, testPassword);
      const userId = userResponse.rows[0].id;
      const testCategoryName = 'unitTestCategory123456TestTest5555';
      await database.addCategory(userResponse.rows[0].id, testCategoryName);
      const categoryResponse = await database.getCategory(userId, testCategoryName);
      const categoryId = categoryResponse.rows[0].id;
      const response = await database.addSubCategory(testName, userId, categoryId);

      const response2 = await database.updateSubCategory(response.rows[0].id, userId, 'testCategoryNewNameInUnitTests123231312', 150);

      expect(response2.command).to.equal('UPDATE');
      expect(response2.rowCount).to.equal(1);
    });

    it('should save activity to database when succesful', async () => {
      const testDate = '2018-05-10';
      const userResponse = await database.getUser(testUsername, testPassword);
      const userId = userResponse.rows[0].id;
      const testPayeeName = 'testPayee';
      const testMemo = 'memo';
      const testAmount = -150;

      const response = await database.addActivity(userId, testPayeeName, testDate, testMemo, testAmount);

      expect(response.command).to.equal('INSERT');
      expect(response.rowCount).to.equal(1);
    });

    it('should return all user activities when succesful', async () => {
      const testDate = '2018-05-10';
      const userResponse = await database.getUser(testUsername, testPassword);
      const userId = userResponse.rows[0].id;
      const testPayeeName = 'testPayee';
      const testMemo = 'memo';

      await database.addActivity(userId, testPayeeName, testDate, testMemo, -150);
      await database.addActivity(userId, testPayeeName, testDate, testMemo, 250);

      const response = await database.getAllUserActivities(userId);

      expect(response.rowCount).to.equal(2);
      expect(response.rows[0].amount).to.equal(-150);
    });

    it('should return all user categories when succesful', async () => {
      const testName = 'test sub category';
      const userResponse = await database.getUser(testUsername, testPassword);
      const userId = userResponse.rows[0].id;
      const testCategoryName = 'unitTestCategory123456TestTest5555';
      await database.addCategory(userResponse.rows[0].id, testCategoryName);
      const categoryResponse = await database.getCategory(userId, testCategoryName);
      const categoryId = categoryResponse.rows[0].id;

      await database.addSubCategory(testName, userId, categoryId);
      await database.addSubCategory(testName, userId, categoryId);

      const response = await database.getAllUserCategories(userId);

      expect(response.rowCount).to.equal(2);
      expect(response.rows[0].name).to.equal(testName);
    });
  });
});

describe('oAuth model', () => {
  const testUsername = 'testUsername';
  const testPassword = 'Salasana123@';

  const testClientId = 'testlientId';
  const testClientSecret = 'testClientSecret';

  before(async () => {
    await database.registerUser(testUsername, testPassword);
  });

  after(async () => {
    await database.removeUser(testUsername);
  });

  it('should return client info object with given clientID', async () => {
    const client = await oAuthModel.getClient(testClientId, testClientSecret);

    expect(client.id).to.equal(testClientId);
  });

  it('should return user info object if user exists and password is correct', async () => {
    const userObject = await oAuthModel.getUser(testUsername, testPassword);

    expect(userObject.username).to.equal(testUsername);
  });

  it('should return promise reject with error if user info is incorrect', async () => {
    await oAuthModel.getUser(testUsername, 'WrongPassword123@').catch((error) => {
      expect(error).to.exist.and.to.be.instanceOf(Error).and.have.property('message', 'User authentication failed.');
    });
  });

  it('should save token when expected', async () => {
    const testToken = { accessToken: '123456-test-access-token' };

    const client = await oAuthModel.getClient(testClientId, testClientSecret);

    const user = await oAuthModel.getUser(testUsername, testPassword);

    await oAuthModel.saveToken(testToken, client, user);

    const savedToken = await oAuthModel.getAccessToken(testToken.accessToken);

    expect(savedToken.accessToken).to.equal(testToken.accessToken);
  });
});

describe('API', () => {
  const testUsername = 'testUsername';
  const testPassword = 'Salasana123@';

  beforeEach(async () => {
    await database.registerUser(testUsername, testPassword);
  });

  afterEach(async () => {
    await database.removeUser(testUsername);
  });

  it('should respond to POST /v1/register', async () => {
    await database.removeUser(testUsername);
    await request(server).post('/v1/register')
      .type('form')
      .send({ _method: 'post', username: testUsername, password: testPassword })
      .then((response) => {

        expect(response.statusCode).to.equal(200);
        expect(response).to.be.json;
      });
  });

  it('should respond to POST /v1/oauth/token', async () => {
    await request(server).post('/v1/oauth/token')
      .type('form')
      .send({
        _method: 'post', grant_type: 'password', username: testUsername, password: testPassword, client_id: 123456, client_secret: 0,
      })
      .then((response) => {
        expect(response.statusCode).to.equal(200);
        expect(response).to.be.json;
        expect(response.body.accessToken).to.exist;
      });
  });

  it('should respond to GET /v1/categories', async () => {
    const testName = 'test sub category';
    const userResponse = await database.getUser(testUsername, testPassword);
    const userId = userResponse.rows[0].id;
    const testCategoryName = 'unitTestCategory123456TestTest5555';
    await database.addCategory(userResponse.rows[0].id, testCategoryName);
    const categoryResponse = await database.getCategory(userId, testCategoryName);
    const categoryId = categoryResponse.rows[0].id;

    await database.addSubCategory(testName, userId, categoryId);
    await database.addSubCategory(testName, userId, categoryId);

    await request(server).post('/v1/oauth/token')
      .type('form')
      .send({
        _method: 'post', grant_type: 'password', username: testUsername, password: testPassword, client_id: 123456, client_secret: 0,
      })
      .then(async (response) => {
        const categories = await request(server).get('/v1/categories')
          .set('Authorization', `Bearer ${response.body.accessToken}`)
          .send();

        expect(categories.statusCode).to.equal(200);
        expect(categories).to.be.json;
        expect(categories.body[0].name).to.equal(testName);
      });
  });

  it('should respond to PUT /v1/categories', async () => {
    const testName = 'test sub category';
    const userResponse = await database.getUser(testUsername, testPassword);
    const userId = userResponse.rows[0].id;
    const testCategoryName = 'unitTestCategory123456TestTest5555';
    await database.addCategory(userResponse.rows[0].id, testCategoryName);
    const categoryResponse = await database.getCategory(userId, testCategoryName);
    const categoryId = categoryResponse.rows[0].id;

    await request(server).post('/v1/oauth/token')
      .type('form')
      .send({
        _method: 'post', grant_type: 'password', username: testUsername, password: testPassword, client_id: 123456, client_secret: 0,
      })
      .then(async (response) => {
        const response2 = await request(server).put('/v1/categories')
          .type('json')
          .set('Authorization', `Bearer ${response.body.accessToken}`)
          .send({ name: testName, parentCategory: categoryId, budgeted: 77 });

        expect(response2.statusCode).to.equal(200);
        expect(response2).to.be.json;

        const category = await database.getSubCategory(userId, testName);

        expect(category.rows[0].name).to.equal(testName);
      });

    await database.addSubCategory(testName, userId, categoryId);
    await database.addSubCategory(testName, userId, categoryId);
  });

  it('should respond to GET /v1/activities', async () => {
    const testDate = '2018-05-10';
    const userResponse = await database.getUser(testUsername, testPassword);
    const userId = userResponse.rows[0].id;
    const testPayeeName = 'testPayee';
    const testMemo = 'memo';
    const testAmount = -150;

    await database.addActivity(userId, testPayeeName, testDate, testMemo, testAmount);
    await database.addActivity(userId, testPayeeName, testDate, testMemo, testAmount);

    await request(server).post('/v1/oauth/token')
      .type('form')
      .send({
        _method: 'post', grant_type: 'password', username: testUsername, password: testPassword, client_id: 123456, client_secret: 0,
      })
      .then(async (response) => {
        const categories = await request(server).get('/v1/activities')
          .set('Authorization', `Bearer ${response.body.accessToken}`)
          .send();

        expect(categories.statusCode).to.equal(200);
        expect(categories).to.be.json;
        expect(categories.body[0].payee).to.equal(testPayeeName);
      });
  });

  it('should respond to PUT /v1/activities', async () => {
    const testDate = '2019-01-01';
    const userResponse = await database.getUser(testUsername, testPassword);
    const testPayeeName = 'testPayee2';
    const testMemo = 'Test memo memo unit testing activities';
    const testAmount = -353;

    await request(server).post('/v1/oauth/token')
      .type('form')
      .send({
        _method: 'post', grant_type: 'password', username: testUsername, password: testPassword, client_id: 123456, client_secret: 0,
      })
      .then(async (response) => {
        const response2 = await request(server).put('/v1/activities')
          .type('json')
          .set('Authorization', `Bearer ${response.body.accessToken}`)
          .send({
            payeeName: testPayeeName, date: testDate, memo: testMemo, amount: testAmount,
          });

        expect(response2.statusCode).to.equal(200);
        expect(response2).to.be.json;

        const activity = await database.getActivity(response.body.user.id, response2.body.id);

        expect(activity.rows[0].payee).to.equal(testPayeeName);
      });
  });
});
