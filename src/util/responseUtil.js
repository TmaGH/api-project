const debug = true;

class ResponseContext {
  constructor(options) {
    this.options = options;
  }

  response(res) {
    this.response.success = function (jsonBody, status = 200, headers) {
      if (headers !== undefined && headers !== null) {
        Object.keys(headers).forEach(headerKey => res.append(headerKey, headers[headerKey]));
      }

      this.success.send = function () {
        res.status(status).json(jsonBody);
      };

      return this.success;
    };

    this.response.error = function (name, message, status = 400, headers) {
      if (headers !== undefined && headers !== null) {
        Object.keys(headers).forEach(headerKey => res.append(headerKey, headers[headerKey]));
      }

      const resObject = { error: { name, message } };

      this.error.debug = function (debugObject) {
        if (debug === true) {
          resObject.error.debug = debugObject;
        }

        return this;
      };

      this.error.send = function () {
        res.status(status).json(resObject);
      };

      return this.error;
    };

    return this.response;
  }
}

module.exports = new ResponseContext({ debug: true });
