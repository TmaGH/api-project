import { Pool } from 'pg';

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'TestiDB',
  password: 'Salasana',
  port: 5433,
});

function query(queryString, values) {
  return pool.connect().then(client => client.query(queryString, values).then(
    (res) => {
      client.release();
      return res;
    },
  ).catch((err) => {
    client.release();
    return err;
  }));
}

module.exports = {
  query,

};
