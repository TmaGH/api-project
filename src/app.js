import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import logger from 'morgan';

import auth from './module/authModule';

import v1Routes from './routes/v1Routes';

const app = express();

// Logging
app.use(logger('dev'));

// BodyParser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// CookieParser
app.use(cookieParser());

// Static dir
app.use(express.static(path.join(__dirname, 'public')));

// oAuth2 server object
app.oauth = auth.oauthServer;

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  // intercepts OPTIONS method
  if (req.method === 'OPTIONS') {
    // respond with 200
    res.sendStatus(200);
  } else {
    // move on
    next();
  }
});

// Versioning, look into specified routes file for actual routes
app.use('/v1', v1Routes);

module.exports = app;
