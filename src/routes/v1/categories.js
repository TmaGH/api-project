import resUtil from '../../util/responseUtil';
import dbModule from '../../module/databaseModule';

function getCategories(res, token) {
  dbModule.getAllUserCategories(token.user.id).then((response) => {
    resUtil.response(res).success(response.rows).send();
  }).catch(err => resUtil.response(res).error(err.name, err.message, err.code).debug({ filename: 'categories.js', line: 7, error: err }).send());
}

function putCategories(res, body, token) {

  console.log(body);

  dbModule.addSubCategory(body.name, token.user.id, body.parentCategory, body.budgeted)
    .then((response) => {
      if (response.rowCount > 0) {
        resUtil.response(res).success({ id: response.rows[0].id }).send();
      } else {
        resUtil.response(res).error('database_error', 'Adding category failed.').debug({ filename: 'categories.js', line: 16, error: response }).send();
      }
    }).catch(err => resUtil.response(res).error(err.name, err.message, err.code).debug(err).send());
}

function postCategories(res, body, token) {

  console.log(body);

  dbModule.updateSubCategory(body.id, token.user.id, body.updateName, body.updateBudgeted)
    .then((response) => {
      if (response.rowCount > 0) {
        resUtil.response(res).success().send();
      } else {
        resUtil.response(res).error('database_error', 'Updating category failed.').debug({ filename: 'categories.js', line: 33, error: response }).send();
      }
    }).catch(err => resUtil.response(res).error(err.name, err.message, err.code).debug(err).send());
}

/*
function deleteCategories() {

}
*/

function handle(req, res) {
  const { method } = req;
  const { user } = res.locals;

  switch (method) {
    case 'GET': case 'HEAD':
      getCategories(res, user);
      break;
    case 'PUT':
      putCategories(res, req.body, user);
      break;
    case 'POST':
      postCategories(res, req.body, user);
      break;
    default:
      resUtil.response(res).error('not_supported', `HTTP method ${method} is not supported.`, 405, { Allow: 'GET, HEAD, PUT' }).send();
  }
}

module.exports = {
  handle,
};
