import resUtil from '../../util/responseUtil';
import dbModule from '../../module/databaseModule';

function getActivities(res, token) {
  dbModule.getAllUserActivities(token.user.id).then((response) => {
    resUtil.response(res).success(response.rows).send();
  }).catch(err => resUtil.response(res).error(err.name, err.message, err.code).debug({ filename: 'activities.js', line: 7, error: err }).send());
}

function putActivities(res, body, token) {
  dbModule.addActivity(token.user.id, body.payeeName, body.date, body.memo, body.amount)
    .then((response) => {
      if (response.rowCount > 0) {
        resUtil.response(res).success({ id: response.rows[0].id }).send();
      } else {
        resUtil.response(res).error('database_error', 'Adding activity failed.').debug({ filename: 'activities.js', line: 16, error: response }).send();
      }
    }).catch(err => resUtil.response(res).error(err.name, err.message, err.code).debug(err).send());
}

/*
function postActivities() {

}


function deleteActivities() {

}
*/

function handle(req, res) {
  const { method } = req;
  const { user } = res.locals;

  switch (method) {
    case 'GET': case 'HEAD':
      getActivities(res, user);
      break;
    case 'PUT':
      putActivities(res, req.body, user);
      break;
    default:
      resUtil.response(res).error('not_supported', `HTTP method ${method} is not supported.`, 405, { Allow: 'GET, HEAD, PUT' }).send();
  }
}

module.exports = { handle };
