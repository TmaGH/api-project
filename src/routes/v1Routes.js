import express from 'express';
import auth from '../module/authModule';

import categories from './v1/categories';
import activities from './v1/activities';

const oauth = auth.oauthServer;
const router = express.Router();

router.post('/register', auth.register);

router.all('/oauth/token', oauth.token());

router.all('/categories', oauth.authenticate(), categories.handle);

router.all('/activities', oauth.authenticate(), activities.handle);

module.exports = router;
