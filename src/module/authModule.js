import OAuth2Server from 'oauth2-server';
import oAuthModel from './model/oAuthModel';
import dbModule from './databaseModule';
import resUtil from '../util/responseUtil';

class MyOAuthServer {
  constructor(options) {
    this.server = new OAuth2Server(options);
  }

  token() {
    const { server } = this;
    return (req, res) => {
      const request = new OAuth2Server.Request(req);
      const response = new OAuth2Server.Response(res);

      server
        .token(request, response, { requireClientAuthentication: { password: false } })
        .then(token => resUtil.response(res).success(token).send())
        .catch(err => resUtil.response(res).error(err.name, err.message).debug({ filename: 'authModule.js', line: 20, error: err }).send());
    };
  }

  authenticate(options) {
    const { server } = this;
    return (req, res, next) => {
      const request = new OAuth2Server.Request({
        headers: { authorization: req.headers.authorization },
        method: req.method,
        query: req.query,
        body: req.body,
      });
      const response = new OAuth2Server.Response(res);

      server.authenticate(request, response, options)
        .then((token) => {
          // Request is authorized.
          res.locals.user = token;
          next();
        })
        .catch((err) => {
          // Request is not authorized.
          resUtil.response(res).error(err.name, err.message, err.code).debug({ filename: 'authModule.js', line: 43, error: err }).send();
        });
    };
  }
}

function register(req, res) {
  console.log(req.body);

  dbModule.registerUser(req.body.username, req.body.password).then(async (response) => {
    if (response.rowCount === 1) {
      const userId = response.rows[0].id;

      const categ1 = (await dbModule.addCategory(userId, 'Immediate')).rows[0].id;
      const categ2 = (await dbModule.addCategory(userId, 'Debt')).rows[0].id;
      const categ3 = (await dbModule.addCategory(userId, 'Other')).rows[0].id;
      await dbModule.addSubCategory('Rent', userId, categ1, 0);
      await dbModule.addSubCategory('Student loan', userId, categ2, 0);
      await dbModule.addSubCategory('Vacation', userId, categ3, 0);

      resUtil.response(res).success().send();
    } else {
      resUtil.response(res).error('database_error', 'Username already exists.').debug(response).send();
    }
  }).catch((err) => {
    resUtil.response(res).error(err.name, err.message).debug(err).send();
  });
}

const oauthServer = new MyOAuthServer({
  model: oAuthModel,
});

module.exports = { register, oauthServer };
