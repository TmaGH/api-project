/* todo: Implement oAuth module with Redis */

import dbModule from '../databaseModule';

// Temporary in-memory storage method for development
const accessTokens = {};

function getClient(id) {
  const client = {
    id,
    grants: ['password'],
    redirectUris: null,
  };

  return new Promise((resolve) => {
    resolve(client);
  });
}

function getUser(username, password) {
  return dbModule.getUser(username, password).then(response => new Promise((resolve, reject) => {
    if (response.rowCount === 1) {
      const userObject = {};

      userObject.id = response.rows[0].id;
      userObject.username = response.rows[0].username;

      resolve(userObject);
    } else {
      reject(new Error('User not found.'));
    }
  }));
}

function saveToken(token, client, user) {

  const savedToken = {};

  savedToken.accessToken = token.accessToken;
  savedToken.accessTokenExpiresAt = token.accessTokenExpiresAt;
  savedToken.refreshToken = token.refreshToken;
  savedToken.refreshTokenExpiresAt = token.refreshTokenExpiresAt;
  // savedToken.scope = token.scope;
  savedToken.client = client;
  savedToken.user = user;

  accessTokens[token.accessToken] = savedToken;

  return Promise.resolve(savedToken);
}

function getAccessToken(accessToken) {
  return Promise.resolve(accessTokens[accessToken]);
}

module.exports = {
  getClient, getUser, saveToken, getAccessToken,
};
