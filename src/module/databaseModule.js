import dbUtil from '../util/pgUtil';

function isString(x) {
  return Object.prototype.toString.call(x) === '[object String]';
}

function authenticateUser(username, password) {
  const authenticateQuery = 'SELECT crypt($2, password) = password AS accepted FROM account WHERE username = $1';
  const values = [username, password];
  return dbUtil.query(authenticateQuery, values).then((response) => {
    if (response.rowCount === 0) return false;
    return response.rows[0].accepted;
  });
}

function registerUser(username, password) {
  // At least 8 characters, uppercase and lower case letters, a number and special character
  // https://regex101.com/r/zh4cff/1
  const passwordReqRegExp = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,})');

  // Validation
  if (!isString(username) || !isString(password)) return Promise.reject(new Error('Empty username or password.'));
  if (!passwordReqRegExp.test(password)) return Promise.reject(new Error('Password does not fulfill strength requirements.'));

  const registerUserQuery = 'WITH x AS (SELECT $1::text AS "user", $2::text AS pw, gen_salt(\'bf\')::text AS salt) INSERT INTO account (username, password) SELECT x.user, crypt(x.pw, x.salt) FROM x RETURNING id';
  const values = [username, password];
  return dbUtil.query(registerUserQuery, values);
}

function getUser(username, password) {
  return this.authenticateUser(username, password).then((success) => {
    if (success === true) {
      const getUserQuery = 'SELECT * FROM account WHERE username = $1';
      const values = [username];
      return dbUtil.query(getUserQuery, values);
    }
    return Promise.reject(new Error('User authentication failed.'));
  });
}

function removeUser(username) {
  const removeUserQuery = 'DELETE FROM account WHERE username = $1';
  const values = [username];
  return dbUtil.query(removeUserQuery, values);
}

function userExists(username) {
  const doesUserExistQuery = 'SELECT $1 = username AS exists FROM account WHERE username = $1';
  const values = [username];
  return dbUtil.query(doesUserExistQuery, values).then((response) => {
    if (response.rows === undefined) return response;
    if (response.rowCount === 0) return false;
    return response.rows[0].exists;
  });
}

function addCategory(userId, name) {
  const addCategoryQuery = 'INSERT INTO category (account, name) VALUES ($1, $2) RETURNING id';
  const values = [userId, name];
  return dbUtil.query(addCategoryQuery, values);
}

function getCategory(userId, name) {
  const getCategoryQuery = 'SELECT * FROM category WHERE account = $1 AND name = $2';
  const values = [userId, name];
  return dbUtil.query(getCategoryQuery, values);
}

// For testing only
function getSubCategory(userId, name) {
  const getCategoryQuery = 'SELECT * FROM sub_category WHERE account = $1 AND name = $2';
  const values = [userId, name];
  return dbUtil.query(getCategoryQuery, values);
}

function getActivity(userId, activityId) {
  const getActivityQuery = 'SELECT activity.id, activity.date, activity.memo, activity.amount, payee.name AS payee, payee.id AS payee_id, payee.category FROM activity, payee WHERE activity.account = $1 AND activity.id = $2';
  const values = [userId, activityId];
  return dbUtil.query(getActivityQuery, values);
}

function getAllUserCategories(userId) {
  const getAllCategoriesQuery = 'SELECT sub_category.id, sub_category.name, sub_category.budgeted, category.name AS parent_category, category.id AS parent_id FROM sub_category, category WHERE sub_category.account = $1 AND sub_category.parent_category = category.id ORDER BY category.id';
  const values = [userId];
  return dbUtil.query(getAllCategoriesQuery, values);
}

function getAllUserActivities(userId) {
  const getAllUserActivitiesQuery = 'SELECT activity.id, activity.date, activity.memo, activity.amount, payee.name AS payee, payee.id AS payee_id, payee.category FROM activity, payee WHERE activity.account = $1 AND activity.payee = payee.id ORDER BY date';
  const values = [userId];
  return dbUtil.query(getAllUserActivitiesQuery, values);
}

function updateSubCategory(id, userId, newName, newBudgeted) {
  const updateSubCategoryQuery = 'UPDATE sub_category SET (name, budgeted) = ($1, $2) WHERE id = $3 AND account = $4';
  const values = [newName, newBudgeted, id, userId];
  return dbUtil.query(updateSubCategoryQuery, values);
}

function addSubCategory(name, userId, categoryId, budgeted) {
  let addSubCategoryQuery;
  let values;

  if (budgeted === undefined) {
    addSubCategoryQuery = 'INSERT INTO sub_category (name, account, parent_category) VALUES ($1, $2, $3) RETURNING id';
    values = [name, userId, categoryId];
  } else {
    addSubCategoryQuery = 'INSERT INTO sub_category (name, account, parent_category, budgeted) VALUES ($1, $2, $3, $4) RETURNING id';
    values = [name, userId, categoryId, budgeted];
  }

  return dbUtil.query(addSubCategoryQuery, values);
}

function getPayee(name) {
  const getPayeeQuery = 'SELECT * FROM payee WHERE name = $1';
  const values = [name];
  return dbUtil.query(getPayeeQuery, values);
}

function addPayee(userId, name, category) {
  let addPayeeQuery;
  let values;

  if (category === undefined) {
    addPayeeQuery = 'INSERT INTO payee (account, name) VALUES ($1, $2)';
    values = [userId, name];
  } else {
    addPayeeQuery = 'INSERT INTO payee (account, name, category) VALUES ($1, $2, $3)';
    values = [userId, name, category];
  }

  return dbUtil.query(addPayeeQuery, values);
}

function addActivity(userId, payeeName, date, memo, amount) {
  return getPayee(payeeName).then((response) => {
    let promise;
    if (response.rowCount === 1) {
      const payeeId = response.rows[0].id;

      const addActivityQuery = 'INSERT INTO activity (account, payee, date, memo, amount) VALUES ($1, $2, $3, $4, $5) RETURNING id';
      const values = [userId, payeeId, date, memo, amount];

      promise = dbUtil.query(addActivityQuery, values);
    } else {
      promise = addPayee(userId, payeeName).then((response2) => {
        if (response2.rowCount === 1) {
          return addActivity(userId, payeeName, date, memo, amount);
        }

        console.log(response2);

        return Promise.reject(new Error('Adding payee failed.'));
      });
    }
    return promise;
  });
}


module.exports = {
  authenticateUser,
  registerUser,
  getUser,
  removeUser,
  userExists,
  addCategory,
  getCategory,
  getSubCategory,
  getActivity,
  addSubCategory,
  addActivity,
  getAllUserCategories,
  getAllUserActivities,
  updateSubCategory,
};
