import Vue from 'vue';
import Vuex from 'vuex';
import {
  getCategories, getActivities, updateCategory, addCategory, addActivity,
} from './api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: undefined,
    categoriesSorted: {},
    activities: [{ date: 'something' }],
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    addEmptyCategory(state, payload) {
      if (state.categoriesSorted[payload.parentName] === undefined) {
        state.categoriesSorted[payload.parentName] = [];
      }

      state.categoriesSorted[payload.parentName].push({
        id: payload.id,
        name: '',
        budgeted: '0',
        parent_category: payload.parentName,
        parent_id: payload.parentCategory,
      });
    },
    addCategory(state, payload) {
      if (state.categoriesSorted[payload.parent] === undefined) {
        state.categoriesSorted[payload.parent] = [];
      }

      state.categoriesSorted[payload.parent].push(payload.item);
    },
    editCategory(state, payload) {
      state.categoriesSorted[payload.parent][payload.index] = payload.item;
    },
    setCategories(state, categories) {
      state.categoriesSorted = categories;
    },
    setActivities(state, activities) {
      state.activities = activities;
    },
    addActivity(state, payload) {
      state.activities.push(payload);
    },
  },
  actions: {
    getCategories(context, token) {
      return getCategories(token.accessToken)
        .then((response) => {
          const categories = response.data;
          categories.forEach(categ => context.commit('addCategory', {
            parent: categ.parent_category,
            item: categ,
          }));
        });
    },
    getActivities(context, token) {
      return getActivities(token.accessToken)
        .then((response) => {
          const activities = response.data;
          context.commit('setActivities', activities);
        });
    },
    updateCategory(context, params) {
      return updateCategory(
        params.token.accessToken,
        params.id,
        params.updateName,
        params.updateBudgeted,
      )
        .then((response) => {
          console.log(response);
          context.commit('editCategory', params.commitObject);
        });
    },
    addEmptyCategory(context, params) {
      return addCategory(params.token.accessToken, 'New Category', params.parentCategory, '0')
        .then((response) => {
          console.log(response);
          context.commit('addEmptyCategory', { id: response.data.id, parentName: params.parentName, parentCategory: params.parentCategory });
        });
    },
    addActivity(context, params) {
      return addActivity(params.token.accessToken, params.payee, params.date, params.memo, params.amount)
        .then((response) => {
          console.log(response);
          context.commit('addActivity', {
            id: response.data.id, category: '1', date: params.date, payee: params.payee, memo: params.memo, amount: params.amount, payee_id: '999',
          });
        });
    },
  },
});
