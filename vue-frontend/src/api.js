import axios from 'axios';
import qs from 'qs';

const baseUrl = 'http://localhost:3000/v1';

function register(username, password) {
  return axios({
    method: 'post',
    url: `${baseUrl}/register`,
    data: {
      username,
      password,
    },
  });
}

function login(username, password) {
  const data = qs.stringify({
    grant_type: 'password',
    username,
    password,
    client_id: '123456',
  });

  return axios({
    method: 'post',
    url: `${baseUrl}/oauth/token`,
    data,
    headers: {
      'Content-type': 'application/x-www-form-urlencoded',
    },
  });
}

function getCategories(accessToken) {
  return axios({
    method: 'get',
    url: `${baseUrl}/categories`,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

function getActivities(accessToken) {
  return axios({
    method: 'get',
    url: `${baseUrl}/activities`,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

function updateCategory(accessToken, id, updateName, updateBudgeted) {
  return axios({
    method: 'post',
    url: `${baseUrl}/categories`,
    data: {
      id,
      updateName,
      updateBudgeted,
    },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

function addCategory(accessToken, name, parentCategory, budgeted) {
  return axios({
    method: 'put',
    url: `${baseUrl}/categories`,
    data: {
      name,
      parentCategory,
      budgeted,
    },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

function addActivity(accessToken, payeeName, date, memo, amount) {
  return axios({
    method: 'put',
    url: `${baseUrl}/activities`,
    data: {
      payeeName,
      date,
      memo,
      amount,
    },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

export {
  register, login, getCategories, getActivities, updateCategory, addCategory, addActivity,
};
