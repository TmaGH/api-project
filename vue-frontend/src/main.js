// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'vuetify/dist/vuetify.min.css';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import Vuetify from 'vuetify';
import App from './App';
import router from './router';
import store from './store';

Vue.use(Vuetify, {
  theme: {
    primary: '#03a9f4',
    secondary: '#9c27b0',
    accent: '#cddc39',
    error: '#f44336',
    warning: '#8bc34a',
    info: '#00bcd4',
    success: '#009688',
  },
});
Vue.use(VeeValidate);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
