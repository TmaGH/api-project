CREATE EXTENSION pgcrypto;

create table account
(
  id       bigserial   not null
    constraint account_pkey
    primary key,
  username varchar(50) not null,
  password varchar(72) not null
);

alter table account
  owner to postgres;

create unique index user_username_uindex
  on account (username);

create table category
(
  id      bigserial    not null
    constraint category_pkey
    primary key,
  name    varchar(150) not null,
  account bigserial    not null
    constraint category_account_id_fk
    references account
    on delete cascade
);

alter table category
  owner to postgres;

create unique index category_name_uindex
  on category (name);

create table sub_category
(
  id              bigserial         not null
    constraint sub_category_pkey
    primary key,
  name            varchar(150)      not null,
  budgeted        integer default 0 not null,
  parent_category bigserial         not null
    constraint sub_category_category_id_fk
    references category,
  account         bigserial         not null
    constraint sub_category_account_id_fk
    references account
    on delete cascade
);

alter table sub_category
  owner to postgres;

create table payee
(
  id       bigserial    not null
    constraint payee_pkey
    primary key,
  name     varchar(150) not null,
  category bigserial
    constraint payee_sub_category_id_fk
    references sub_category
    on update cascade on delete set null,
  account  bigserial    not null
    constraint payee_account_id_fk
    references account
    on delete cascade
);

alter table payee
  owner to postgres;

create table activity
(
  id      bigserial not null
    constraint activity_pkey
    primary key,
  date    date      not null,
  memo    varchar(500),
  amount  integer   not null,
  account bigserial not null
    constraint activity_account_id_fk
    references account
    on delete cascade,
  payee   bigserial not null
    constraint activity_payee_id_fk
    references payee
);

alter table activity
  owner to postgres;

create unique index payee_name_uindex
  on payee (name);