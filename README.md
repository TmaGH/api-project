Budjetointi API rajapintakuvaus

Käyttöohjeet

Lähdekoodi sijaitsee osoitteessa https://gitlab.com/TmaGH/api-project.
API sijauitsee projektin ylimmässä kansiossa ja se perustuu Node.js järjestelmään. API saadaan käytiin ”npm run start” komennolla. Testit voidaan ajaa ”npm run test” komennolla.
Oletuksena API pyörii portissa 3000. Localhostissa sitä kutsutaan osoitteesta ”localhost:3000/”.
Kaikki tällä hetkellä tehdyt endpointit sijaitsevat v1 versionnin alla eli osoitteessa ”localhost:3000/v1/”.

Käyttöliittymä sijaitsee vue-frontend kansiossa. Se saadaan käyntiin ”npm run dev” komennolla tässä kansiossa.

API

API:n kaikki endpointit ja HTTP-metodit:
Endpoint	GET	POST	PUT
/v1/register	Ei tuettu	Rekisteröi käyttäjätunnuksen	Ei tuettu
/v1/oauth/token	Ei tuettu	Palauttaa oAuth2 tokenin	Ei tuettu
/v1/categories	Palauttaa kaikki kategoriat	Päivittää kategoriaa	Lisää kategorian
/v1/activities	Palauttaa kaikki aktiviteetit	Ei tuettu	Lisää aktiviteetin

Virhetilanteessa (status code jokin muu kuin 200), vastauksen body osassa löytyy lisätietoa virheestä.
Virhetietojen formaatti on aina sama. Status on yleensä 400 tai jos ongelma liittyy authentikaatioon 403 tai 401. Siitä löytyvät JSON objektin kohdat ovat seuraavat:
	name: virheen koodi
message: selittävä teksti siitä mikä virhe tapahtui ja miksi se tapahtui (voidaan lauttaa suoraan käyttöliittymään)
	debug (vain debug tilassa): lisätietoa ohjelmoijalle virheen konteksista’
Authentikaatio suoritetaan hakemalla tokeni v1/oauth/token endpointista ja lisäämällä se authentikaatio vaativaan kutsuun headeriksi Authorization: Bearer {accessToken}.
Endpoint kuvauksissa mainitaan, jos endpointin käyttäminen vaatii tämän headerin.


POST /v1/register

Rekisteröinnin body voidaan lähettää Form URL Encoded tai JSON muodossa.
Parametrit:
	username: uusi käyttäjätunnus
	password: uuden käyttäjän salasana
Palautus:
Jos rekistöinti onnistuu, API palauttaa HTTP statuksen 200.

POST /v1/oauth/token

Tokenin hakemisessa parametrit täytyy lähettää Form URL Encoded muodossa! oAuth2 serveri ei hyväksy mitään muuta muotoa.
Parametrit:
	username: käyttäjätunnus
	password: salasana
Palautus:
Onnistuessa HTTP status 200 ja JSON objekti, mikä sisältää esim. accessToken muuttujan.

GET /v1/categories

Kutsun tekeminen vaatii authentikaatio headerin.
Parametrit:
Parametreja ei tällä hetkellä ole.
Palautus:
Palauttaa kaikki authentikoidun käyttäjän kategoriat.

POST /v1/categories

Kutsun tekeminen vaatii authentikaatio headerin.
Parametrit:
	id: päivitettävän kategorian id
	updateName: kategorian uusi nimi
	updateBudgeted: kategorian uusi budjetoitu summa
Palautus:
Palauttaa status code 200 jos päivitys onnistui.

PUT /v1/categories

Kutsun tekeminen vaatii authentikaatio headerin.
Parametrit:
	name: kategorian nimi
	parentCategorry: minkä pääkategorian alikategoriaksi laitetaan
	budgeted: budjetoitu summa

GET /v1/activities

Kutsun tekeminen vaatii authentiaatio headerin.
Parametrit:
Parametreja ei tällä hetkellä ole.
Palautus:
Palauttaa kaikki authentikoidun käyttäjän aktiviteetit.

PUT /v1/activities

Kutsun tekeminen vaatii authentikaatio headerin.
Parametrit:
	payeeName: maksaja / maksun vastaanottaja
	date: maksun päiväys (yyyy-mm-dd)
	memo: maksun kuvaus / vapaa kenttä
	amount: maksun määrä
